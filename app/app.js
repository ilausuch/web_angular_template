/**
Angular application template
author: Ivan Lausuch Sales
Started on 2014
*/

console.info("----------------- New execution -----------------");

//----------------------------------------------------------------------------------
// ANGULAR APPLICATION
//----------------------------------------------------------------------------------

//TODO : All all needed modules
//NOTE: In mobile apps is required ["ui.router"]
var app = angular.module('app', ["ui.router","pascalprecht.translate",'smart-table']);

//----------------------------------------------------------------------------------
// TRANSLATE
//----------------------------------------------------------------------------------
app.config(['$translateProvider', function($translateProvider) {
	$translateProvider.useStaticFilesLoader({
		prefix: 'resources/translate/locale-',
		suffix: '.json'
	});

	$translateProvider.preferredLanguage('en');
}]);
    
//----------------------------------------------------------------------------------
// MAIN CONTROLER
//----------------------------------------------------------------------------------
//Create default main controller
var mc;

app.controller("MainController", function($rootScope,$scope,$timeout,$http,$q,$translate){

	//Set mc global variable
	mc=this;
	
	//Easy access to main controls
	this.$rootScope=$rootScope;
	this.$scope=$scope;
	this.$timeout=$timeout;
	this.$http=$http;
	this.$q=$q;
	this.c=undefined;
	this.route={};

	//Complete main controller with functions
	completeMainController();
	
	this.changeLang=function(lang){
		$translate.use(lang)
	}
	
	//TODO : Define here all variables or functions you need
	
	//TODO : onReady event. It is called when all are ready
	this.onReady=function(){
		console.info("app/app.js - MainController - onReady - Implement here user check or other needs");
	}
	
	/* This is an example */
	this.rowCollection = [
        {firstName: 'Laurent', lastName: 'Renard', birthDate: new Date('1987-05-21'), balance: 102, email: 'whatever@gmail.com'},
        {firstName: 'Blandine', lastName: 'Faivre', birthDate: new Date('1987-04-25'), balance: -2323.22, email: 'oufblandou@gmail.com'},
        {firstName: 'Francoise', lastName: 'Frere', birthDate: new Date('1955-08-27'), balance: 42343, email: 'raymondef@gmail.com'}
    ];
	
	//Force to start Main controller
	mc.start();
});

		
//----------------------------------------------------------------------------------
// ROUTER CONTROLER
//----------------------------------------------------------------------------------

/**
This is route configuration. Is where you define all routes in your application
*/
app.config(function($stateProvider, $urlRouterProvider) {
    //Default route
    $urlRouterProvider.otherwise('/');
    
    //TODO : Define all routes ($stateProvider, route(string), controller(string) [, persistent(bool) ,prerequisitesChecker(func)])
    /*
	NOTE:PrerequisitesChecker must be a funcion with these params (callback,$stateParams,$location) 
	and mush call to callback when finish
	*/
    stateRouteAdd($stateProvider,"/","index",true,
    	function(callback,$stateParams,$location){
	    	console.info("app/app.js - Route config - / - Prerequisites checker (remove if you don't need it)");
	    	callback();	
	    }
	);  
	
	stateRouteAdd($stateProvider,"/page2","page2");
	
	
});
